import React from "react";


export default function ListOfTask({addedTask,addInput,addTask,taskList}) {

    return(
        <div>
            <h2>List of Tasks</h2>
            <input placeholder="Type in your new task" value={addedTask} onChange={addInput}/>
            <button onClick={addTask}>Add Tasks</button>
            {taskList}
        </div>
    )
}