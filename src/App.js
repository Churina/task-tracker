import React from "react"
import './App.css';
import ListOfTask from "./ListOfTask";
import ToDoList from "./ToDoList";


function App() {

  const [taskArray, setTaskArray] = React.useState([
    "Daily stand up",
    "Meet with manager",
    "Meet with customer",
    "Reply Email",
    "Attend production meeting",
    "Write journal",
    "Check next day's schedule",
    "Ask question about any unclear thing"
  ])

  const [toDoArray, setTodoArray] = React.useState([])
  const [addedTask, setAddedTask] = React.useState("");


  function addTask(){
    if(addedTask !== ""){
      setTaskArray(preState => [...preState, addedTask]);
      setAddedTask("");
    }
 }


  function addInput(event){
      setAddedTask(event.target.value);
  }


  function addTodo(task) {
    setTodoArray(preState => [...preState, task])
  }


  function deleteTodo(todo) {
    const deletedTodoIndex = toDoArray.indexOf(todo)
    const newToDoArray = [...toDoArray]
    newToDoArray.splice(deletedTodoIndex, 1)
    setTodoArray(newToDoArray)
  }



  const taskList = taskArray.map(task => {
      return (
      <div>
        <span>{task}</span>
        <button onClick={()=>addTodo(task)}>add</button>
      </div>
      )
  } );


  const toDoList = toDoArray.map(todo => {
    return (
      <div>
        <span>{todo}</span>
        <button onClick={()=>deleteTodo(todo)}>Completed</button>
      </div>
    )
  });


  return (
    <div className="container">
      <ListOfTask addedTask={addedTask} addInput={addInput} addTask={addTask} taskList={taskList} />
      <ToDoList toDoList={toDoList} />
    </div>
    
  );
}

export default App;
