import React from "react";

export default function ToDoList({toDoList}){
    return(
        <div>
            <h2>To Do Lists</h2>
            {toDoList}
        </div>
    )
}