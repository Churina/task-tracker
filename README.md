                                               **Task tracker exercise v1**

Catalyte wants to build a simple application to help associates track common tasks they work on during the training cycle. An associate should be able to select from a list of tasks to add to their TODO list. Once the task is completed, it should be removed
from the TODO list.

- Requirements:

1. The application should display a list of at least 8 possible tasks for associates to choose from (ex. Daily Standup).
2. Selecting a task should add a copy of the task to the associate's TODO list.
3. Associates should be able to complete a task on their TODO list, which removes it from the TODO list.
4. Application should be broken down into multiple, logical components, stored in separate files.
5. Component state should be updated immutably.

- Stretch Requirements (These are optional requirements for additional practice):

1. Create a way for associates to enter and add their own tasks, which will be added to the list of possible task options to choose from.
